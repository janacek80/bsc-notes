import * as React from 'react';
import styled from 'styled-components';

interface LanguageProps {
  language: string;
  current: boolean;
  onClick: (language: string) => void;
}

const LanguageChanger = styled.span`
  margin-left: 10px;
  padding-left: 10px;
  text-transform: uppercase;
  cursor: pointer;
  border-left: 1px solid #DDD;

  &.current {
    font-weight: bold;
  }

  &:first-of-type {
    border-left: 0;
  }
`;

const Language = ({ language, current, ...props }: LanguageProps) => {

  const onClick = (e: any) => {
    props.onClick(language);
  };

  return (
    <LanguageChanger className={current ? 'current' : ''} onClick={onClick}>
      {language}
    </LanguageChanger>
  );
};

export default Language;