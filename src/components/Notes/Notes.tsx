import React from 'react';
import { navigate } from '@reach/router';
import { useSelector, useDispatch } from 'react-redux'
import { Table, Button } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import NotesRow from './components/NotesRow';
import { StyledH1 } from '../../styles';
import { Note } from '../../store/notesList/types';
import DeleteDialog from './components/DeleteDialog';
import { deleteNote } from '../../store/notesList/actions';

interface NotesProps {
  path?: string;
}

const Notes = (props: NotesProps) => {
  
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const notes = useSelector((state: any) => state && state.notesList && state.notesList.notes);

  const [deleteNoteId, setDeleteNoteId] = React.useState(0);

  const onCreateClick = () => {
    navigate('/note');
  };

  const onClickDelete = (noteId: number) => {
    setDeleteNoteId(noteId);
  };

  const handleDeleteSubmit = () => {
    dispatch(deleteNote(deleteNoteId));
    setDeleteNoteId(0);
  };

  const handleDeleteCancel = () => {
    setDeleteNoteId(0);
  };

  return (
    <main>
      <StyledH1>{t('Notes')}</StyledH1>
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>{t('Title')}</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {notes && notes.map((note: Note) => (
            <NotesRow key={note.id} note={note} onClickDelete={onClickDelete} />
          ))}
        </tbody>
      </Table>
      <Button variant="primary" onClick={onCreateClick}>{t('Create new note')}</Button>
      <DeleteDialog
        show={deleteNoteId !== 0}
        handleSubmit={handleDeleteSubmit}
        handleClose={handleDeleteCancel}
      />
    </main>
  );
};

export default Notes;