export const translationsCs = {
  translation: {
    "Notes": "Poznámky",
    "Title": "Nadpis",
    "Create new note": "Vytvořit novou poznámku",
    "Delete": "Smazat",
    "Delete note": "Smazat poznámku ?",
    "Are you sure you want to delete this note?": "Opravdu chcete smazat tuto poznámku?",
    "Close": "Zavřít",
    "Note detail": "Detail poznámky",
    "Note title": "Nadpis poznámky",
    "Submit": "Odeslat",
    "Cancel": "Zrušit"
  }
};