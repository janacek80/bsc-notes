import {
  NotesListState,
  NotesActionTypes,
  ADD_NOTE_TO_LIST,
  UPDATE_NOTE_IN_LIST,
  REMOVE_NOTE_FROM_LIST,
  SET_NOTE_LIST,
  Note
} from './types';

export const notesListInitialState: NotesListState = {
  notes: [],
}

export const notesListReducer = (
  state = notesListInitialState,
  action: NotesActionTypes,
): NotesListState => {
  switch (action.type) {
    
    case ADD_NOTE_TO_LIST:
      const newNotes = [...state.notes];
      newNotes.push(action.payload);
      return {
        notes: newNotes
      };

    case UPDATE_NOTE_IN_LIST:
      return {
        notes: [...state.notes].map((n: Note) => {
          if (n.id === action.payload.id) {
            n.title = action.payload.title;
          }
          return n;
        })
      };

    case REMOVE_NOTE_FROM_LIST:
      return {
        notes: [...state.notes].filter((n: Note) => n.id !== action.payload)
      };

    case SET_NOTE_LIST:
      return {
        notes: [...action.payload],
      };

    default:
      return state;
  };
};
