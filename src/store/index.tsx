import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { notesListReducer } from './notesList/reducer'

const reducer = combineReducers({
  notesList: notesListReducer,
})

export const configureStore = () => {
  return createStore(reducer, applyMiddleware(thunkMiddleware))
}
