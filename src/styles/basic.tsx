import styled from 'styled-components';

export const AppContainer = styled.div`
  margin: 15px 0;
`;

export const StyledH1 = styled.h1`
  margin: 0;
  margin-bottom: 15px;
  padding: 0;
  font-size: 30px;
`;