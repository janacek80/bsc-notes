import * as React from 'react';
import { Link } from '@reach/router';
import { Button } from 'react-bootstrap';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';

import { Note } from '../../../../store/notesList/types';

interface NoteRowProps {
  note: Note;
  onClickDelete: (noteId: number) => void;
}

const DeleteCell = styled.td`
  padding: 0;
  padding-top: 9px;
  text-align: right;
`;

const NotesRow = ({ note, ...props }: NoteRowProps) => {
  
  const { t } = useTranslation();

  const onClickDelete = (e: any) => {
    props.onClickDelete(note.id);
  };

  return (
    <tr>
      <td>{note.id}</td>
      <td><Link to={`/note/${note.id}`}>{note.title}</Link></td>
      <DeleteCell>
        <Button variant="danger" size="sm" onClick={onClickDelete}>{t('Delete')}</Button>
      </DeleteCell>
    </tr>
  );
};

export default NotesRow;