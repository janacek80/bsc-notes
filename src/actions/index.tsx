const increaseCount = (state = []) => {
  return {
    type: 'INCREASE_COUNT',
    value: 1
  };
};

const decrease = () => {

};

const reset = () => {

};

export {
  increaseCount,
  decrease,
  reset,
}