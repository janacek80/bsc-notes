import * as React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

interface DeleteDialogProps {
  show: boolean;
  handleSubmit: () => void;
  handleClose: () => void;
}

const DeleteDialog = ({ show, handleSubmit, handleClose }: DeleteDialogProps) => {
  
  const { t } = useTranslation();

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{t('Delete note')}</Modal.Title>
      </Modal.Header>
      <Modal.Body>{t('Are you sure you want to delete this note?')}</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          {t('Close')}
        </Button>
        <Button variant="danger" onClick={handleSubmit}>
          {t('Delete')}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default DeleteDialog;