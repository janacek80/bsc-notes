import * as React from 'react';
import styled from 'styled-components';
import Language from './components/Language';

interface LanguagesProps {
  languages: string[];
  current: string;
  onChange: (language: string) => void;
}

const LanguagesChanger = styled.div`
  text-align: right;
`;

const Languages = ({ languages, current, ...props }: LanguagesProps) => {

  const onChange = (l: string) => {
    props.onChange(l)
  }

  return (
    <LanguagesChanger>
      {languages.map((l: string) => (
        <Language key={l} language={l} current={current === l} onClick={onChange} />
      ))}
    </LanguagesChanger>
  );
};

export default Languages;