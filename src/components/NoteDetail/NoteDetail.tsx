import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Button } from 'react-bootstrap';
import { navigate } from '@reach/router';
import { useTranslation } from 'react-i18next';

import { StyledH1 } from '../../styles';
import { createNote, updateNote } from '../../store/notesList/actions';
import { Note } from '../../store/notesList/types';

interface NoteDetailProps {
  path?: string;
  noteId?: string;
}

const NoteDetail = ({ noteId, ...props }: NoteDetailProps) => {
  
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const notes = useSelector((state: any) => state && state.notesList && state.notesList.notes);
  const selectedNote = notes.find((note: Note) => noteId && note?.id?.toString() === noteId);

  const [data, setData] = React.useState({
    id: selectedNote ? selectedNote.id : null,
    title: selectedNote ? selectedNote.title : '',
  });

  const onCancelClick = () => {
    navigate('/');
  };

  const onSubmit = (e: any) => {
    if (selectedNote) {
      dispatch(updateNote(selectedNote.id, data));
    } else {
      dispatch(createNote(data));
    }
    e.preventDefault();
  };

  const onChange = (e: any) => {
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
  };

  return (
    <>
      <StyledH1>{t('Note detail')}</StyledH1>
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="formTitle">
          <Form.Label>{t('Note title')}</Form.Label>
          <Form.Control
            type="text"
            name="title"
            value={data.title}
            required={true}
            autoFocus={true}
            placeholder={t('Note title')}
            onChange={onChange}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          {t('Submit')}
        </Button>{' '}
        <Button variant="light" onClick={onCancelClick}>
          {t('Cancel')}
        </Button>
      </Form>
    </>
  );
};

export default NoteDetail;