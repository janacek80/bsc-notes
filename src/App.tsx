import React, { useEffect } from 'react';
import { Router } from '@reach/router';
import { useDispatch } from 'react-redux';
import { Container, Row, Col } from 'react-bootstrap';
import i18n from 'i18next';
import { useTranslation, initReactI18next } from 'react-i18next';

import Notes from './components/Notes';
import { fetchNotesList } from './store/notesList/actions';
import NoteDetail from './components/NoteDetail';
import Languages from './components/Languages';
import { translationsEn, translationsCs } from './languages';

i18n
  .use(initReactI18next)
  .init({
    resources: {
      en: translationsEn,
      cs: translationsCs
    },
    lng: "en",
    fallbackLng: "en",
    whitelist: ["en", "cs"],

    interpolation: {
      escapeValue: false
    }
  });

const App = () => {

  const { i18n } = useTranslation();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchNotesList());
  });

  const languages = ['en', 'cs'];

  const changeLanguage = (lang: string) => {
    i18n.changeLanguage(lang);
  };

  return (
    <Container>
      <Row>
        <Col>
          <Languages
            languages={languages}
            current={i18n.language}
            onChange={changeLanguage}
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <Router>
            <Notes path="/" />
            <NoteDetail path="/note/:noteId" />
            <NoteDetail path="/note" />
          </Router>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
