export const translationsEn = {
  translation: {
    "Notes": "Notes",
    "Title": "Title",
    "Create new note": "Create new note",
    "Delete": "Delete",
    "Delete note": "Delete note ?",
    "Are you sure you want to delete this note?": "Are you sure you want to delete this note?",
    "Close": "Close",
    "Note detail": "Note detail",
    "Note title": "Note title",
    "Submit": "Submit",
    "Cancel": "Cancel"
  }
};