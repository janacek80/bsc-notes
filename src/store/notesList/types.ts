export interface Note {
  id: number;
  title: string;
}

export interface NotesListState {
  notes: Note[];
}

export const ADD_NOTE_TO_LIST = 'ADD_NOTE_TO_LIST';
export const UPDATE_NOTE_IN_LIST = 'UPDATE_NOTE_IN_LIST';
export const SET_NOTE_LIST = 'SET_NOTE_LIST';
export const REMOVE_NOTE_FROM_LIST = 'REMOVE_NOTE_FROM_LIST';

interface AddToNoteListAction {
  type: typeof ADD_NOTE_TO_LIST;
  payload: Note;
}

interface UpdateNoteInListAction {
  type: typeof UPDATE_NOTE_IN_LIST;
  payload: Note;
}

interface RemoveNotesListAction {
  type: typeof REMOVE_NOTE_FROM_LIST;
  payload: number;
}

interface SetNotesListAction {
  type: typeof SET_NOTE_LIST;
  payload: Note[];
}

export type NotesActionTypes = AddToNoteListAction | UpdateNoteInListAction | RemoveNotesListAction | SetNotesListAction;