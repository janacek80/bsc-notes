# Instalace

Z časových důvodů nejsou ve vypracování úlohy použity žádné testy. Plánováno bylo použití `jest`.

Postup na spuštění aplikace:

1. Stažení aplikace z gitu/bitucketu
2. Instalace závislostí příkazem `yarn`
3. Definice konfigurace v souboru `.env`, možno použít ze šablony `.env.template`
4. Spuštění aplikace příkazem `yarn start`