import axios from 'axios';
import { Action } from 'redux';
import { navigate } from '@reach/router';
import { ThunkAction } from 'redux-thunk';
import {
  ADD_NOTE_TO_LIST,
  UPDATE_NOTE_IN_LIST,
  REMOVE_NOTE_FROM_LIST,
  SET_NOTE_LIST,
  NotesListState,
  Note
} from './types';

const addToNoteList = (data: Note) => {
  return {
    type: ADD_NOTE_TO_LIST,
    payload: data
  };
};

const updateNoteInList = (data: Note) => {
  return {
    type: UPDATE_NOTE_IN_LIST,
    payload: data
  };
};

const setNotesList = (notes: Note[]) => {
  return {
    type: SET_NOTE_LIST,
    payload: notes,
  };
};

const removeFromNoteList = (noteId: number) => {
  return {
    type: REMOVE_NOTE_FROM_LIST,
    payload: noteId,
  };
};

interface SaveNoteData {
  title: string;
}

export const createNote = (
  data: SaveNoteData
): ThunkAction<void, NotesListState, null, Action<string>> => async dispatch => {
  axios.post(process.env.REACT_APP_API_URL + 'notes', {
    title: data.title,
  })
    .then(({ data }) => {
      dispatch(addToNoteList(data));
      navigate('/');
    })
    .catch(error => {
      console.error('Error creating note', error);
    });
};

export const updateNote = (
  id: number,
  data: SaveNoteData
): ThunkAction<void, NotesListState, null, Action<string>> => async dispatch => {
  axios.put(process.env.REACT_APP_API_URL + `notes/${id}`, {
    title: data.title,
  })
    .then(({ data }) => {
      dispatch(updateNoteInList(data));
      navigate('/');
    })
    .catch(error => {
      console.error('Error updating note', error);
    });
};

export const deleteNote = (
  id: number
): ThunkAction<void, NotesListState, null, Action<string>> => async dispatch => {
  axios.delete(process.env.REACT_APP_API_URL + `notes/${id}`)
    .then(({ data }) => {
      dispatch(removeFromNoteList(id));
    })
    .catch(error => {
      console.error('Error deleting note', error);
    });
};

export const fetchNotesList = (): ThunkAction<void, NotesListState, null, Action<string>> => async dispatch => {
  axios.get(process.env.REACT_APP_API_URL + 'notes')
    .then(({ data }) => {
      dispatch(setNotesList(data));
    })
    .catch(error => {
      console.error('Error loading notes list', error);
    });
};